//
//  iPhoneDiagnostics_v_0_1App.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

@main
struct iPhoneDiagnostics_v_0_1App: App {
    var body: some Scene {
        WindowGroup {
            FeedView()
        }
    }
}
