//
//  LightSensorView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct LightSensorView: View {
    var body: some View {
        Text("Light Sensor View")
    }
}

struct LightSensorView_Previews: PreviewProvider {
    static var previews: some View {
        LightSensorView()
    }
}
