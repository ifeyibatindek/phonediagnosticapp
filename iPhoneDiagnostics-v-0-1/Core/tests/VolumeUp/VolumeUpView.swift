//
//  VolumeUpView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct VolumeUpView: View {
    var body: some View {
        Text("Volume Up!")
    }
}

struct VolumeUpView_Previews: PreviewProvider {
    static var previews: some View {
        VolumeUpView()
    }
}
