//
//  FlashView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct FlashView: View {
    var body: some View {
        Text("Flash View")
    }
}

struct FlashView_Previews: PreviewProvider {
    static var previews: some View {
        FlashView()
    }
}
