//
//  FingerprintSensorView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI
import LocalAuthentication

public var isUnlocked = false
public var biometricState = ""


struct FingerprintSensorView: View {
    
    @State public var etatFingerPrint : String = ""

    var body: some View {
                
        VStack{
            Image("fingerprint")
                .resizable()
                .frame(width:100, height:100)
            
            Text(biometricState)
        }
        .onAppear(perform: authenticate)
        
    }

    func authenticate() {
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
            let reason = "We need to unlock your data."

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                
                if success {
                    biometricState = "Débloqué"
                } else {
                    biometricState = "Bloqué"
                }
            }
        } else {
            biometricState = "Aucune biométrie détectée"
        }
        
    }

}

struct FingerprintSensorView_Previews: PreviewProvider {
    static var previews: some View {
        FingerprintSensorView()
    }
}
