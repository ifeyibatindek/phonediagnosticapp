//
//  PowerButtonView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct PowerButtonView: View {
    var body: some View {
        Text("Power Button")
    }
}

struct PowerButtonView_Previews: PreviewProvider {
    static var previews: some View {
        PowerButtonView()
    }
}
