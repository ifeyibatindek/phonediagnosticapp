//
//  BluetoothManager.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 18/05/2022.
//

import Foundation
import CoreBluetooth
import UIKit

class BluetoothManager: UIViewController, CBCentralManagerDelegate{
    
    var centralManager: CBCentralManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    var bluetoothDescription: CBManagerState {
        return CBManagerState.poweredOn
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
                case .poweredOn:
                    print("ON")
                case .poweredOff:
                    print("OFF")
                case .resetting:
                    print("Resetting")
                case .unauthorized:
                    print("Unauthorized")
                case .unsupported:
                    print("Unsupported")
                case .unknown:
                    print("Unknown")
            @unknown default:
                print("Unknown")
            }    
    }
}
