//
//  BluetoothView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI
import CoreBluetooth

struct BluetoothView: View {
    
    var bluetoothManager = BluetoothManager()

    
    var body: some View {
        VStack {
            Image("bluetooth")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.black)
                        
            Text("Bluetooth: ")
                    .foregroundColor(.black)
                    .font(.system(size: 18, weight: .semibold))
                    .multilineTextAlignment(.center)
                    .padding()
            
            HStack {
                    
                NavigationLink {

                    BluetoothView()
                    
                } label: {
                    
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .foregroundColor(.green)
                        .frame(width: 100, height: 100)
                }
                
                NavigationLink {
                    
                    BluetoothView()
                    
                } label: {
                    
                        Image(systemName: "xmark.circle.fill")
                            .resizable()
                            .foregroundColor(.red)
                            .frame(width: 100, height: 100)
                }
                    
            }
        }
    }
}

struct BluetoothView_Previews: PreviewProvider {
    static var previews: some View {
        BluetoothView()
    }
}
