//
//  BluetoothManager.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 18/05/2022.
//

import Foundation
import CoreLocation
import UIKit

class GPSManager: ObservableObject{
  
    var locationManager = CLLocationManager()
    var locationString = ""
    
    var locationState: String {
        
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
                    locationString = "Pas d'accès"
                case .authorizedAlways, .authorizedWhenInUse:
                    locationString = "Accès"
                @unknown default:
                    break
            }
        } else {
            
            locationString = "Location services are not enabled"
        }
        
        return locationString
    }
}
