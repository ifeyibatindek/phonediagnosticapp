//
//  GPSView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI
import CoreLocation

struct GPSView: View {
    
    @ObservedObject var gpsManager = GPSManager()
    
    var body: some View {
        VStack {
            Image("gps")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.black)
            
            Text((gpsManager.locationState))
                .foregroundColor(.black)
                .font(.system(size: 18, weight: .semibold))
                .multilineTextAlignment(.center)
                .padding()
            
            HStack {
                    
                NavigationLink {

                    BluetoothView()
                    
                } label: {
                    
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .foregroundColor(.green)
                        .frame(width: 100, height: 100)
                }
                
                NavigationLink {
                    
                    BluetoothView()
                    
                } label: {
                    
                        Image(systemName: "xmark.circle.fill")
                            .resizable()
                            .foregroundColor(.red)
                            .frame(width: 100, height: 100)
                }
                    
            }
        }
    }
}

struct GPSView_Previews: PreviewProvider {
    static var previews: some View {
        GPSView()
    }
}
