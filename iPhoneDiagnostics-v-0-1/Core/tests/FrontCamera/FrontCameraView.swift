//
//  FrontCameraView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct FrontCameraView: View {
    var body: some View {
        Text("Front Camera View")
    }
}

struct FrontCameraView_Previews: PreviewProvider {
    static var previews: some View {
        FrontCameraView()
    }
}
