//
//  WifiView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct WifiView: View {
    
    @ObservedObject var networkManager = NetworkManager()

    var body: some View {
        VStack {
            Image(systemName: networkManager.imageName)
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.black)
            
            Text(networkManager.connectionDescription)
                .foregroundColor(.black)
                .font(.system(size: 18, weight: .semibold))
                .multilineTextAlignment(.center)
                .padding()
            
            if !networkManager.isConnected {
                Button {
                    print("Handle action..")
                } label: {
                    Text("Réessayer")
                        .padding()
                        .font(.headline)
                        .foregroundColor(Color(.systemBlue))
                }
                .frame(width: 140)
                .background(Color.white)
                .clipShape(Capsule())
                .padding()
            }
            
            HStack {
                    
                NavigationLink {
                    
                    FeedView()
                    
                } label: {
                    
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .foregroundColor(.green)
                        .frame(width: 100, height: 100)
                }
                
                NavigationLink {
                    
                    FeedView()
                    
                } label: {
                    
                        Image(systemName: "xmark.circle.fill")
                            .resizable()
                            .foregroundColor(.red)
                            .frame(width: 100, height: 100)
                }
                    
            }
        }
    }
}

struct WifiView_Previews: PreviewProvider {
    static var previews: some View {
        WifiView()
    }
}
