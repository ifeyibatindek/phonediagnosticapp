//
//  HeadphonesView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI
import AVFoundation
import AVKit

var audioPlayer2: AVAudioPlayer!

struct HeadphonesView: View {
    
    @State var changePage : Bool = false
    @State var volume: Float = 0

    
    var body: some View {
        
        NavigationView {
        
            VStack {
                
                Image(systemName: "speaker.wave.3")
                    .resizable()
                    .frame(width: 100, height: 100)
                Text("Did you hear the song clearly?")
                    .font(.title)
                    .padding(.bottom, 20.0)
                        
                Text("current volume is \(volume)")
                .onReceive(AVAudioSession.sharedInstance().publisher(for: \.outputVolume), perform: { value in
                    self.volume = value
                })
                
                HStack {
                        
                    NavigationLink {

                        _DTouchView()
                        
                    } label: {
                        
                        Image(systemName: "checkmark.circle.fill")
                            .resizable()
                            .foregroundColor(.green)
                            .frame(width: 100, height: 100)
                    }
                    
                    NavigationLink {
                        
                        _DTouchView()
                        
                    } label: {
                        
                            Image(systemName: "xmark.circle.fill")
                                .resizable()
                                .foregroundColor(.red)
                                .frame(width: 100, height: 100)
                    }
                }
            }
            .onAppear{
                self.playSound()
            }
        }
    }
    
    func playSound() {
        
        let url = Bundle.main.url(forResource: "song", withExtension: "mp3")
        
        guard url != nil else{
            return
        }
        do {
            audioPlayer2 = try AVAudioPlayer(contentsOf: url!)
            audioPlayer2?.play()
        
        } catch {
            print("error")
        }
    }
    
    func pauseSound() {
        
        let url = Bundle.main.url(forResource: "song", withExtension: "mp3")
        
        guard url != nil else{
            return
        }
        do {
            audioPlayer2 = try AVAudioPlayer(contentsOf: url!)
            audioPlayer2?.stop()
        
        } catch {
            print("error")
        }
    }
}

struct HeadphonesView_Previews: PreviewProvider {
    static var previews: some View {
        HeadphonesView()
    }
}
