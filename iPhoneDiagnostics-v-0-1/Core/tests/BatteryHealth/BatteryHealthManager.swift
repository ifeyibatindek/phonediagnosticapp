//
//  BatteryHealth.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 18/05/2022.
//

import Foundation
import UIKit

class BatteryHealthManager: ObservableObject {
    
    let queue = DispatchQueue(label: "BatteryHealthManager")
    
    var batteryLevel: Float {
        UIDevice.current.isBatteryMonitoringEnabled = true
        return UIDevice.current.batteryLevel
    }

}
