//
//  BatteryHealthView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct BatteryHealthView: View {
    
    @ObservedObject var batteryHealthManager = BatteryHealthManager()
    
    var body: some View {
        VStack {
            Image("battery-capacity")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.black)
            
                
                Text("\(batteryHealthManager.batteryLevel)")
                    .foregroundColor(.black)
                    .font(.system(size: 18, weight: .semibold))
                    .multilineTextAlignment(.center)
            
            
            HStack {
                    
                NavigationLink {

                    BluetoothView()
                    
                } label: {
                    
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .foregroundColor(.green)
                        .frame(width: 100, height: 100)
                }
                
                NavigationLink {
                    
                    BluetoothView()
                    
                } label: {
                    
                        Image(systemName: "xmark.circle.fill")
                            .resizable()
                            .foregroundColor(.red)
                            .frame(width: 100, height: 100)
                }
                    
            }
        }
    }
}

struct BatteryHealthView_Previews: PreviewProvider {
    static var previews: some View {
        BatteryHealthView()
    }
}
