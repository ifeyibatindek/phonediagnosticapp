//
//  RapportView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 20/05/2022.
//

import LocalAuthentication
import SwiftUI

var etatFingerPrint: String = ""

struct RapportView: View {
    
    @ObservedObject var networkManager = NetworkManager()
    @ObservedObject var gpsManager = GPSManager()
    @ObservedObject var batteryHealthManager = BatteryHealthManager()
    
    var body: some View {
        
        ScrollView {
            LazyVStack {

                Text("Résultat des tests")
                
                Group {
                    HStack {
                        Text("Batterie : "+"\(batteryHealthManager.batteryLevel)")
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    
                    HStack {

                        Text("Fingerprint : "+etatFingerPrint)
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    
                    HStack {

                        Text("Wifi : "+networkManager.connectionDescription)
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    
                    HStack {
                        Text("Compass : "+gpsManager.locationState)
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    HStack {
                        Text("GPS : "+gpsManager.locationState)
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    HStack {
                        Text("3D Touch : "+treeDTouchState)
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    HStack {
                        Text("Camera : OK")
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                }
                
                Group{
                    
                    HStack {
                        Text("Microphone : OK")
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    HStack {
                        Text("Speakers : OK")
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    HStack {
                        Text("Ecouteurs : OK")
                            .foregroundColor(.black)
                            .font(.title)
                            .padding(.vertical, 1.0)
                    }
                    
                }
            }
        }
        .onAppear(perform: authenticate)

    }
    
    func authenticate() {
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
            let reason = "Nous avons besoin de débloquer votre téléphone."

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                
                if success {
                    etatFingerPrint = "Débloqué"
                } else {
                    etatFingerPrint = "Bloqué"
                }
            }
        } else {
            etatFingerPrint = "Aucune biométrie détectée"
        }
        
    }
}

struct RapportView_Previews: PreviewProvider {
    static var previews: some View {
        RapportView()
    }
}
