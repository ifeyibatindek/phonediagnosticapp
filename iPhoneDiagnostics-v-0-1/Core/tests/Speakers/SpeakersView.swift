//
//  SpeakersView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI
import AVFoundation
import AVKit

var audioPlayer: AVAudioPlayer!

struct SpeakersView: View {
    
    @State var changePage : Bool = false

    var body: some View {
        
        NavigationView {
        
            VStack {
                
                Image(systemName: "speaker.wave.3")
                    .resizable()
                    .frame(width: 100, height: 100)
                Text("Did you hear the song clearly?")
                    .font(.title)
                    .padding(.bottom, 20.0)
                                
                HStack {
                        
                    NavigationLink {

                        CompassView()
                        
                    } label: {
                        
                        Image(systemName: "checkmark.circle.fill")
                            .resizable()
                            .foregroundColor(.green)
                            .frame(width: 100, height: 100)
                    }
                    
                    NavigationLink {
                        
                        CompassView()
                        
                    } label: {
                        
                            Image(systemName: "xmark.circle.fill")
                                .resizable()
                                .foregroundColor(.red)
                                .frame(width: 100, height: 100)
                    }
                        
                }
            }
            .onAppear{
                self.playSound()
            }
        }
    }
    
    func playSound() {
        
        let url = Bundle.main.url(forResource: "song", withExtension: "mp3")
        
        guard url != nil else{
            return
        }
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url!)
            audioPlayer?.play()
        
        } catch {
            print("error")
        }
    }
    
    func pauseSound() {
        
        let url = Bundle.main.url(forResource: "song", withExtension: "mp3")
        
        guard url != nil else{
            return
        }
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url!)
            audioPlayer?.pause()
        
        } catch {
            print("error")
        }
    }
}

struct SpeakersView_Previews: PreviewProvider {
    static var previews: some View {
        SpeakersView()
    }
}
