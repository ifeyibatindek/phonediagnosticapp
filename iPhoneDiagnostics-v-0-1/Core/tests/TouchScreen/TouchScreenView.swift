//
//  TouchScreenView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct TouchScreenView: View {
    var body: some View {
        
        VStack {
            Text("Touch Screen!")
        
            HStack {
                Button {
                    
                } label: {
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .foregroundColor(.green)
                        .frame(width: 100, height: 100)
                }
                
                Button {
                    
                } label: {
                    Image(systemName: "xmark.circle.fill")
                        .resizable()
                        .foregroundColor(.red)
                        .frame(width: 100, height: 100)
                }
            }
        }
        
    }
}

struct TouchScreenView_Previews: PreviewProvider {
    static var previews: some View {
        TouchScreenView()
    }
}
