//
//  ThreeDTouchManager.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 18/05/2022.
//

import Foundation
import Network
import UIKit
import SwiftUI

var treeDTouchState = "Absent"

class ThreeDTouchManager: UIViewController, ObservableObject {
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)

        if let touch = touches.first {
            if view.traitCollection.forceTouchCapability == .available {
                if touch.force == touch.maximumPossibleForce {
                    treeDTouchState = "OK"
                }
            }
        }
    }
}
