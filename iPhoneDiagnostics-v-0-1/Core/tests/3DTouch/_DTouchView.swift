//
//  3DTouchView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct _DTouchView: View {
    var body: some View {
        Image(systemName: "hand.tap.fill")
            .resizable()
            .foregroundColor(.black)
            .frame(width: 100, height: 100)
            .contextMenu
            {
                Button(action: { print("Action 1 triggered") }, label:
                {
                    Text(treeDTouchState)
                })
                
            }
    }
    
}

struct _DTouchView_Previews: PreviewProvider {
    static var previews: some View {
        _DTouchView()
    }
}
