//
//  VolumeDownView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI
import AVFoundation
import MediaPlayer

struct VolumeDownView: View {
        
    var body: some View {
                
        VStack {
            Text("Volume Down !")
            
            HStack {
                Button {
                    
                } label: {
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .foregroundColor(.green)
                        .frame(width: 100, height: 100)
                }
                
                Button {
                    
                } label: {
                    Image(systemName: "xmark.circle.fill")
                        .resizable()
                        .foregroundColor(.red)
                        .frame(width: 100, height: 100)
                }
            }
        }
        .onAppear{
            audioPlayer?.stop()
            audioPlayer2?.stop()
        }
    }
}

struct VolumeDownView_Previews: PreviewProvider {
    static var previews: some View {
        VolumeDownView()
    }
}
