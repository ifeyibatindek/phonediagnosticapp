//
//  PixelDeadManager.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 18/05/2022.
//

import Foundation
import Network

class PixelDeadManager: ObservableObject {
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "PixelDeadManager")
    @Published var isConnected = true
    
    var imageName: String {
        return isConnected ? "wifi" : "wifi.slash"
    }
    
    var connectionDescription: String {
        if isConnected {
            return "Wifi Connecté!"
        } else {
            return "Vous n'êtes pas connecté au wifi!"
        }
    }
    
    init() {
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.async {
                self.isConnected = path.status == .satisfied
            }
        }
        
        monitor.start(queue: queue)
    }
}
