//
//  PixelDeadView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct PixelDeadView: View {
    var body: some View {
        Text("Pixel dead View")
    }
}

struct PixelDeadView_Previews: PreviewProvider {
    static var previews: some View {
        PixelDeadView()
    }
}
