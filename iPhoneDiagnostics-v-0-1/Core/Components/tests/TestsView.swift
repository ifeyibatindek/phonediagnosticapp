//
//  TestsView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI
import AVFoundation

struct TestsView: View {
    var body: some View {
        
        HStack {
            VStack(){
                Image("touch-screen")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    TouchScreenView()
                } label: {
                    Text("Touch screen")
                        .font(.body).bold()
                }
            }
            Spacer()
            
            VStack(){
                Image("3d-touch")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    _DTouchView()
                } label: {
                    Text("3D Touch")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("speakers")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    SpeakersView()
                } label: {
                    Text("Speakers")
                        .font(.body).bold()
                }
            }
        }

        HStack {
            VStack(){
                Image("microphone")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    MicrophoneView()
                } label: {
                    Text("Microphone")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("headphones")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    HeadphonesView()
                } label: {
                    Text("headphones")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("compass")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    CompassView()
                } label: {
                    Text("Compass")
                        .font(.body).bold()
                }
            }
        }
        
        HStack {
            VStack(){
                Image("wifi")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    WifiView()
                } label: {
                    Text("Wifi")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("bluetooth")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    BluetoothView()
                } label: {
                    Text("Bluetooth View")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("gps")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    GPSView()
                } label: {
                    Text("GPS")
                        .font(.body).bold()
                }
            }
        }
        
        HStack {
            VStack(){
                Image("light-sensor")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    LightSensorView()
                } label: {
                    Text("Light Sensor")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("charger")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    ChargerView()
                } label: {
                    Text("Charger")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("fingerprint")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    FingerprintSensorView()
                } label: {
                    Text("FingerPrint")
                        .font(.body).bold()
                }
            }
        }
     
        HStack {
            VStack(){
                Image("front-camera")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    CameraPreview(session: AVCaptureSession())
                } label: {
                    Text("Front Camera")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("back-camera")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    CameraPreview(session: AVCaptureSession())
                } label: {
                    Text("Back Camera")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("flash")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    FlashView()
                } label: {
                    Text("Flash")
                        .font(.body).bold()
                }
            }
        }
       
        HStack {
            VStack(){
                Image("battery-capacity")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    BatteryCapacityView()
                } label: {
                    Text("Battery Capacity")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("battery-health")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    BatteryHealthView()
                } label: {
                    Text("Battery Health")
                        .font(.body).bold()
                }
            }
            
            Spacer()
            
            VStack(){
                Image("dead-pixel")
                    .resizable()
                    .frame(width:100, height:100)
                NavigationLink {
                    PixelDeadView()
                } label: {
                    Text("Dead Pixel")
                        .font(.body).bold()
                }
            }
        }
    
        HStack {
             VStack(){
                 Image("volume-down")
                     .resizable()
                     .frame(width:100, height:100)
                 NavigationLink {
                     VolumeDownView()
                 } label: {
                     Text("Volume down")
                         .font(.body).bold()
                 }
             }
             
             Spacer()
             
             VStack(){
                 Image("volume-up")
                     .resizable()
                     .frame(width:100, height:100)
                 NavigationLink {
                     VolumeUpView()
                 } label: {
                     Text("Volume Up")
                         .font(.body).bold()
                 }
             }
             
             Spacer()
             
             VStack(){
                 Image("power")
                     .resizable()
                     .frame(width:100, height:100)
                 NavigationLink {
                     PowerButtonView()
                 } label: {
                     Text("Power Button")
                         .font(.body).bold()
                 }
             }
        } 
        
    }
}

struct TestsView_Previews: PreviewProvider {
    static var previews: some View {
        TestsView()
    }
}
