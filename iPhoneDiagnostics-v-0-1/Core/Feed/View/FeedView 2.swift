//
//  FeedView.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 17/05/2022.
//

import SwiftUI

struct FeedView: View {
        
    let threeColumnGrid = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        
        NavigationView {

            ScrollView {
                LazyVStack {
                  
                    TestsView()
                        .padding(.bottom, 60.0)
                    
                    NavigationLink {
                        
                        RapportView()
                        
                    } label: {
                        Text("Générer le rapport")
                            .font(.subheadline).bold()
                            .frame(width: 120, height: 32)
                            .foregroundColor(.black)
                            .overlay(RoundedRectangle(cornerRadius:20).stroke(Color.gray, lineWidth: 0.75))
                    }
                }
                .padding()

            }
        }
    }
}

struct FeedView_Previews: PreviewProvider {
    static var previews: some View {
        FeedView()
    }
}
