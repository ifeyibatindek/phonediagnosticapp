//
//  AudioPlayerService.swift
//  iPhoneDiagnostics-v-0-1
//
//  Created by Ifeyi Batindek on 18/05/2022.
//

import AVFoundation

class Singleton {
    static let sharedInstance = Singleton()
    private var player: AVAudioPlayer?

    func play() {
        guard let url = Bundle.main.url(forResource: "Sound", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)

            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            guard let player = player else { return }

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }

    func stop() {
        player?.stop()
    }
}
